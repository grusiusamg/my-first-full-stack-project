import {Component, OnInit} from '@angular/core';
import {TodoDataService} from '../service/data/todo-data.service';
import {Router} from '@angular/router';

export class Todo {
  constructor(
    public id: number,
    public description: string,
    public done: boolean,
    public targetDate: Date
  ) {
  }
}

@Component({
  selector: 'app-listtodos',
  templateUrl: './listtodos.component.html',
  styleUrls: ['./listtodos.component.css']
})
export class ListtodosComponent implements OnInit {
  todos: Todo[];

  message: string;
  //   = [
  //   new Todo(1, 'ismokti sokti', false, new Date()),
  //   new Todo(2, 'ismokti vairuoti', false, new Date()),
  //   new Todo(3, 'ismokti dainuoti', false, new Date())
  // ];


  constructor(
    private todoService: TodoDataService,
    private router: Router
  ) {
  }

  ngOnInit()
    :
    void {
    this.refreshTodos();
  }

// tslint:disable-next-line:typedef
  refreshTodos() {
    this.todoService.retrieveAllTodos('vardas').subscribe(
      response => {
        console.log(response);
        this.todos = response;
      }
    );
  }

// tslint:disable-next-line:typedef
  deleteTodo(id
               :
               number
  ) {
    console.log(`istrinta ${id}`);
    this.todoService.deleteTodo('vardas', id).subscribe(
      response => {
        console.log(response);
        this.message = `Id nr:${id} istrinta sekmingai!`;
        this.refreshTodos();
      }
    );
  }

// tslint:disable-next-line:typedef
  updateTodo(id
               :
               number
  ) {
    console.log(`update ${id}`);
    this.router.navigate(['todos', id]);
  }
}


