import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {WelcomeDataService} from '../service/data/welcome-data.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  welcomeMessageFromService: string;
  name = '';

  constructor(private route: ActivatedRoute,
              private service: WelcomeDataService) {
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.name = this.route.snapshot.params.name;
  }

  // tslint:disable-next-line:typedef
  getWelcomeMessage() {
    this.service.executeHelloWorldBeanService().subscribe(
      response => this.handleSuccessfulResponse(response),
      error => this.handleErrorResponse(error)
    );
  }

  // tslint:disable-next-line:typedef
  getWelcomeMessageWithParameter() {
    this.service.executeHelloWorldBeanServiceWithPathVariable(this.name).subscribe(
      response => this.handleSuccessfulResponse(response),
      error => this.handleErrorResponse(error)
    );
  }

  // tslint:disable-next-line:typedef
  handleSuccessfulResponse(response) {
    this.welcomeMessageFromService = response.message;
  }

  // tslint:disable-next-line:typedef
  private handleErrorResponse(error) {
    this.welcomeMessageFromService = error.error.message;
  }
}
