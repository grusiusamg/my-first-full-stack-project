import {Component, OnInit} from '@angular/core';
import {HardcodedAuthenticationService} from '../service/hardcoded-authentication.service';

@Component({
  selector: 'app-loggout',
  templateUrl: './loggout.component.html',
  styleUrls: ['./loggout.component.css']
})
export class LoggoutComponent implements OnInit {

  constructor(private hardcodedAuthenticationService: HardcodedAuthenticationService) {
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.hardcodedAuthenticationService.loggout();
  }

}
