import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HardcodedAuthenticationService {

  constructor() {
  }

  // tslint:disable-next-line:typedef
  authenticate(username, password) {
    if (username === 'vardas' && password === '123456') {
      // @ts-ignore
      sessionStorage.setItem('authenticatedUser', username);
      return true;
    }
    return false;
  }

  // tslint:disable-next-line:typedef
  isUserLoggedIn() {
    const user = sessionStorage.getItem('authenticatedUser');
    return !(user === null);
  }

  // tslint:disable-next-line:typedef
  loggout() {
    sessionStorage.removeItem('authenticatedUser');
  }

}
